---
layout: default
title: About
permalink: /about/
---

Site fet a partir del curs **«La fabricació digital per l’àmbit artístic»** impulsat pel 
[Departament d'Educació](https://educacio.gencat.cat/ca/inici/index.html) durant el curs 
escolar 2023-2024 a professors d'art.
 
Aquest site està fet amb **Jekyll** i allotjat al [GitLab](https://www.gitlab.com) 
Hi pots trobar més informació a [Jekyll](https://jekyllrb.com/)

![Jekyll](https://jekyllrb.com/img/logo-2x.png "Jekyll")


