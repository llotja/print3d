---
layout: default
title: Documentació
permalink: /documentacio/
---

## Links i documents

- [Vectoritzacio i fabricació d'elements metàl·Lics del segle XIX. PDF-A4](https://docs.google.com/document/d/e/2PACX-1vTV0HPgFGgVlcw4iKtgb43Aynq58AbtYqpISTOkjpPVaKOjVjVXVAXfYTGAN0fYTSnSqFIDqOWpwsNK/pub)
- [Paràmetres talladora làser per Lídia Sevilla. PDF](https://classroom.google.com/c/NjMzMzA0MTIzMDI4/m/NjMzOTUyNzQyNTE5/details)
