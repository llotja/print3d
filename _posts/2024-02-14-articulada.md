---
layout: curs
title: "Ex05.Peça articulada (II)"
date: 2024-02-14 18:00:00 +0200
author: "Amós P. i Escrig"
---

I aquí el tenim! L'animal articulat de color blanc!
Veure el resultat és mot encoratjador i gràcies a la feina de la nostra professora **Lídia Sevilla** per 
la paciència infinita que ha tingut amb tots els alumnes del curs i d'ajudar-nos en les impressions. 

![animació](/images/animal_articulat.webp)

