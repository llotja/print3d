---
layout: curs
title: "Ex01.Digitalització de patrons metàl·lics"
date: 2023-10-17 18:00:00 +0200
author: "Amós P. i Escrig"
---

## Objectiu

Vectoritzar els motius i tallar-los amb una impresora de tall.

## Introducció

Hi ha poc coneixement de l'existència de llibres digitalitzats sobre disciplines
desaparegudes abans de l'aparició d'internet. L'exercici consisteix en 
**vectoritzar models** i poder-los tallar amb una impresora de tall laser.

## Requeriments

- **Software de vectorització**: ex. [Inkscape](https://wwww.inkscape.org)
- **Models extrets de manuals digitalitzats.** [archive.org](https://www.archive.org)

## Instruccions

1- **Cercar el model**.Primer de tot cal cercar el model per vectoritzar. Pel nostre cas s'han 
extret d'un manual del segle XIX digitalitzat i allotjat a
[archive.org](https://www.archive.org). 

2- **Vectorització**. La vectorització s'ha portat a terme amb [Inkscape](https://wwww.inkscape.org)
procurant fent servir pocs nodes per accelerar la velocitat de la màquina i fent
una separació de traçats amb colors per fer que la màquina també separi 
processos i poder-los fer amb un ordre.

![animació](/images/ezgif-3-1ba54c3f5f.gif)

3- **Preparació de l'arxiu**
	-- El traçat ha d'estar per sota de **0.01 mm**.
	-- No hi ha d'haver contingut al traçat (cast. "relleno").
	-- El document s'ha d'haver guardat amb format **SVG**.

## [+]Info
	
- **«Digitalització de patrons metàl·lics»(PDF)-[A4.B/N]:**[https://docs.google.com/document/d/19ZRozdS0IQVWg2bGnUNYaNBwPXQtUMAjYwqvX0Qfe8c/edit?usp=sharing](https://docs.google.com/document/d/19ZRozdS0IQVWg2bGnUNYaNBwPXQtUMAjYwqvX0Qfe8c/edit?usp=sharing)
- **«Paràmetres talladora làser» by Lídia Sevilla»(PDF):**[https://classroom.google.com/c/NjMzMzA0MTIzMDI4/m/NjMzOTUyNzQyNTE5/details](https://classroom.google.com/c/NjMzMzA0MTIzMDI4/m/NjMzOTUyNzQyNTE5/details)
