---
layout: curs
title: "Artistes i Tecnologia"
date: 2023-10-16 18:00:00 +0200
author: "Amós P. i Escrig"
---
## Objectiu

1. Proposar artistes que facin servir tecnologia CAD.
2. Analitzar les obres i els processos generatius.

## Resum en treball col·laboratiu

Aquí podem veure alguns artistes que fan conses amb 3D
{% include artistes_pw.html %}
