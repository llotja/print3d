---
layout: curs
title: "Ex04.Peça articulada"
date: 2024-01-18 18:00:00 +0200
author: "Amós P. i Escrig"
---

En aquesta sessió es practicarà fer una peça articulada.
La realització es basa en una articulació que uneix dues peces amb una bola a l'interior.

![animació](/images/articulacio_01.png)

